let password1Element = document.getElementById("password1");
let password2Element = document.getElementById("password2");
let icon1Element = document.getElementById("icon1");
let icon2Element = document.getElementById("icon2");

icon1Element.addEventListener("click", function (event) {
    iCanSeeyou(password1Element, icon1Element);
});
icon2Element.addEventListener("click", function (event) {
    iCanSeeyou(password2Element, icon2Element);
});


function iCanSeeyou(input, icon) {
    if (input.type === "password") {
        icon.classList.replace("fa-eye-slash", "fa-eye");
        input.type = "text";
}
    else {
        icon.classList.replace("fa-eye", "fa-eye-slash");
        input.type = "password";
    }
}

let buttonElement = document.getElementById("button");
let divElement = document.createElement("div");
buttonElement.before(divElement);
buttonElement.addEventListener("click", function (event) {
    event.preventDefault();
    if (password1Element.value === password2Element.value) {
        alert("You are welcome");
        divElement.innerText = "";
        password1Element.value = "";
        password2Element.value = "";
    }
    else {
        divElement.innerText = "Потрібно ввести однакові значення";
        divElement.style.color = "red";
    }
})